import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCkxXGjsn_-BYCAhLGGhzYIPxzqTdFgdnE",
  authDomain: "carrental-d8a65.firebaseapp.com",
  databaseURL: "https://carrental-d8a65.firebaseio.com",
  projectId: "carrental-d8a65",
  storageBucket: "carrental-d8a65.appspot.com",
  messagingSenderId: "130388594383",
  appId: "1:130388594383:web:77b80b17ccf2f323ddf1ba",
  measurementId: "G-VN5XRQ00BK",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.firestore().settings({});
export default firebase;
