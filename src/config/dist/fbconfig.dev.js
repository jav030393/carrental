"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _app = _interopRequireDefault(require("firebase/app"));

require("firebase/firestore");

require("firebase/auth");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCkxXGjsn_-BYCAhLGGhzYIPxzqTdFgdnE",
  authDomain: "carrental-d8a65.firebaseapp.com",
  databaseURL: "https://carrental-d8a65.firebaseio.com",
  projectId: "carrental-d8a65",
  storageBucket: "carrental-d8a65.appspot.com",
  messagingSenderId: "130388594383",
  appId: "1:130388594383:web:77b80b17ccf2f323ddf1ba",
  measurementId: "G-VN5XRQ00BK"
}; // Initialize Firebase

_app["default"].initializeApp(firebaseConfig);

_app["default"].firestore().settings({});

var _default = _app["default"];
exports["default"] = _default;