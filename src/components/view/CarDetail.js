import React from "react";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";
import { Link } from "react-router-dom";

const CarDetail = ({ carDetail, carId }) => {
  if (carDetail) {
    return (
      <div className="car-detail__container">
        <div className="car-detail__image">
          <h5>{"$" + carDetail.price}</h5>
          <img src={carDetail.src} alt={carDetail.brand}></img>
        </div>
        <div className="car-detail__details">
          <h3>Detail</h3>
          <div className="car-detail__icons">
            <div className="car-detail__icons-item">
              <img src="/images/user.png" alt="Users"></img>
              <p>{carDetail.person}</p>
            </div>
            <div className="car-detail__icons-item">
              <img src="/images/transmision.png" alt="Transmision"></img>
              <p>{carDetail.transmision}</p>
            </div>
            <div className="car-detail__icons-item">
              <img src="/images/door.png" alt="Door"></img>
              <p>{carDetail.door}</p>
            </div>
          </div>
          <p>{carDetail.text}</p>
        </div>
        <Link className="button__simple" to={"/pay/" + carId}>
          {" "}
          RENT{" "}
        </Link>
      </div>
    );
  } else {
    return (
      <div className="car-detail__center">
        <h1>Loading car detail ...</h1>
      </div>
    );
  }
};

const mapStateToProps = (state, ownProps) => {
  const id = ownProps.match.params.id;
  const cars = state.firestore.data.car;
  const car = cars ? cars[id] : null;
  return {
    carDetail: car,
    carId: id,
  };
};

export default compose(
  connect(mapStateToProps),
  firestoreConnect([{ collection: "car" }])
)(CarDetail);
