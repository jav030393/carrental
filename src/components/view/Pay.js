import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { updateRentHistory } from "../../store/actions/userActions";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";

class Pay extends Component {
  state = {
    getDate: "",
    returnDate: "",
    email: "",
    password: "",
    firstName: "",
    lastName: "",
    birthDate: "",
    identification: "",
    creditCard: "",
    creditCardNumber: "",
  };
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };
  handleSubmit = (e) => {
    e.preventDefault();

    let car = this.props.cars.filter((car) =>
      car.id === this.props.match.params.id ? this : null
    )[0];

    let rentInfo = {
      ...this.props.profile,
      rentHistory: [
        ...this.props.profile.rentHistory,
        {
          src: car.src,
          brand: car.brand,
          getDate: this.state.getDate,
          returnDate: this.state.returnDate,
          total:
            parseInt(
              (new Date(this.state.returnDate) - new Date(this.state.getDate)) /
                (24 * 3600 * 1000)
            ) * car.price,
        },
      ],
    };

    this.props.pay(rentInfo);
  };
  render() {
    if (!this.props.auth.uid) return <Redirect to="/signin" />;
    return (
      <div className="pay__container">
        <form className="pay__form" onSubmit={this.handleSubmit}>
          <div className="pay__rent-date">
            <div className="input__field">
              <label htmlFor="getDate">Get Date</label>
              <input
                type="date"
                id="getDate"
                onChange={this.handleChange}
                required
              />
            </div>
            <span>&#8250;</span>
            <div className="input__field">
              <label htmlFor="returnDate">Return Date</label>
              <input
                type="date"
                id="returnDate"
                onChange={this.handleChange}
                required
              />
            </div>
          </div>

          <div className="input__field">
            <label htmlFor="firstName">First Name</label>
            <input
              type="text"
              id="firstName"
              value={this.props.profile.firstName}
              onChange={this.handleChange}
            />
          </div>
          <div className="input__field">
            <label htmlFor="lastName">Last Name</label>
            <input
              type="text"
              id="lastName"
              value={this.props.profile.lastName}
              onChange={this.handleChange}
            />
          </div>
          <div className="input__field">
            <label htmlFor="birthDate">Birth Date</label>
            <input
              type="date"
              id="birthDate"
              value={this.props.profile.birthDate}
              onChange={this.handleChange}
            />
          </div>
          <div className="input__field">
            <label htmlFor="identification">Identification</label>
            <input
              type="text"
              id="identification"
              value={this.props.profile.identification}
              onChange={this.handleChange}
              required
            />
          </div>
          <div className="input__field">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              value={this.props.auth.email}
              onChange={this.handleChange}
              required
            />
          </div>
          <div className="input__field">
            <label htmlFor="creditCard">Credit Card</label>
            <select id="creditCard" onChange={this.handleChange}>
              <option value="1">Visa</option>
              <option value="2">MasterCard</option>
            </select>
          </div>
          <div className="input__field">
            <label htmlFor="creditCardNumber">Credit Card Number</label>
            <input
              type="text"
              id="creditCardNumber"
              value={this.props.profile.creditCardNumber}
              onChange={this.handleChange}
              required
            />
          </div>
          <div className="pay__button">
            <button className="button__simple" type="submit">
              Pay
            </button>
          </div>
          <div className="pay__success">
            <p>{this.props.payMessage ? this.props.payMessage : null}</p>
          </div>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    pay: (payInfo) => dispatch(updateRentHistory(payInfo)),
  };
};

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
    cars: state.firestore.ordered.car,
    payMessage: state.user.payMessage,
  };
};
export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firestoreConnect([{ collection: "car" }])
)(Pay);
