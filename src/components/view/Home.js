import React, { Component } from "react";
import CarItem from "../car/CarItem";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";

class Home extends Component {
  render() {
    const { cars } = this.props;
    return (
      <div className="home__container">
        <div className="home__filter"></div>
        <div className="home__card-deck">
          {cars &&
            cars.map((car) => {
              return (
                <CarItem
                  src={car.src}
                  alt={car.brand}
                  price={"$" + car.price}
                  key={car.id}
                  link={"/car/" + car.id}
                />
              );
            })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cars: state.firestore.ordered.car,
  };
};

export default compose(
  connect(mapStateToProps),
  firestoreConnect([{ collection: "car" }])
)(Home);
