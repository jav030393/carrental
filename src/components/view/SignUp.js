import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { signUp } from "../../store/actions/authActions";

class SignUp extends Component {
  state = {
    email: "",
    password: "",
    firstName: "",
    lastName: "",
    birthDate: "",
    identification: "",
    creditCard: "",
    creditCardNumber: "",
  };
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.signUp(this.state);
  };
  render() {
    const { authError, auth } = this.props;
    if (auth.uid) return <Redirect to="/" />;
    return (
      <div className="sign-up__container">
        <form className="sign-up__form" onSubmit={this.handleSubmit}>
          <h2>Sign Up</h2>
          <div className="input__field">
            <label htmlFor="firstName">First Name</label>
            <input type="text" id="firstName" onChange={this.handleChange} />
          </div>
          <div className="input__field">
            <label htmlFor="lastName">Last Name</label>
            <input type="text" id="lastName" onChange={this.handleChange} />
          </div>
          <div className="input__field">
            <label htmlFor="birthDate">Birth Date</label>
            <input type="date" id="birthDate" onChange={this.handleChange} />
          </div>
          <div className="input__field">
            <label htmlFor="identification">Identification</label>
            <input
              type="text"
              id="identification"
              onChange={this.handleChange}
            />
          </div>
          <div className="input__field">
            <label htmlFor="email">Email</label>
            <input type="email" id="email" onChange={this.handleChange} />
          </div>
          <div className="input__field">
            <label htmlFor="password">Password</label>
            <input type="password" id="password" onChange={this.handleChange} />
          </div>
          <div className="input__field">
            <label htmlFor="creditCard">Credit Card</label>
            <select id="creditCard" onChange={this.handleChange}>
              <option value="1">Visa</option>
              <option value="2">MasterCard</option>
            </select>
          </div>
          <div className="input__field">
            <label htmlFor="creditCardNumber">Credit Card Number</label>
            <input
              type="text"
              id="creditCardNumber"
              onChange={this.handleChange}
            />
          </div>
          <div className="sign-up__button">
            <button className="button__simple" type="submit">
              Sign Up
            </button>
          </div>
          <div className="sign-up__error">
            <p>{authError ? authError : null}</p>
          </div>
        </form>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    signUp: (newUser) => dispatch(signUp(newUser)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
