import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { updateUser } from "../../store/actions/authActions";

class Profile extends Component {
  state = {
    email: "",
    password: "",
    firstName: "",
    lastName: "",
    birthDate: "",
    identification: "",
    creditCard: "",
    creditCardNumber: "",
  };
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.updateUser(this.state);
  };
  render() {
    if (!this.props.auth.uid) return <Redirect to="/signin" />;
    const { profile } = this.props;
    return (
      <div className="profile__container">
        <form className="profile__form" onSubmit={this.handleSubmit}>
          <div className="input__field">
            <label htmlFor="firstName">First Name</label>
            <input
              type="text"
              id="firstName"
              value={this.props.profile.firstName}
              onChange={this.handleChange}
            />
          </div>
          <div className="input__field">
            <label htmlFor="lastName">Last Name</label>
            <input
              type="text"
              id="lastName"
              value={this.props.profile.lastName}
              onChange={this.handleChange}
            />
          </div>
          <div className="input__field">
            <label htmlFor="birthDate">Birth Date</label>
            <input
              type="date"
              id="birthDate"
              value={this.props.profile.birthDate}
              onChange={this.handleChange}
            />
          </div>
          <div className="input__field">
            <label htmlFor="identification">Identification</label>
            <input
              type="text"
              id="identification"
              value={this.props.profile.identification}
              onChange={this.handleChange}
              required
            />
          </div>
          <div className="input__field">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              value={this.props.auth.email}
              onChange={this.handleChange}
              required
            />
          </div>
          <div className="input__field">
            <label htmlFor="password">Password</label>
            <input type="password" id="password" onChange={this.handleChange} />
          </div>
          <div className="input__field">
            <label htmlFor="creditCard">Credit Card</label>
            <select id="creditCard" onChange={this.handleChange}>
              <option value="1">Visa</option>
              <option value="2">MasterCard</option>
            </select>
          </div>
          <div className="input__field">
            <label htmlFor="creditCardNumber">Credit Card Number</label>
            <input
              type="text"
              id="creditCardNumber"
              value={this.props.profile.creditCardNumber}
              onChange={this.handleChange}
              required
            />
          </div>
          <div className="profile__button">
            <button className="button__simple" type="submit">
              Save
            </button>
          </div>
        </form>
        <div className="profile__rent-history">
          <h3>Rent History</h3>
          <table>
            <thead>
              <tr>
                <th></th>
                <th>Car</th>
                <th>Get Date</th>
                <th>Return Date</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
              {this.props.profile.rentHistory &&
                this.props.profile.rentHistory.map((rent) => {
                  return (
                    <tr>
                      <td>
                        <img src={rent.src} alt={rent.brand}></img>
                      </td>
                      <td>{rent.brand}</td>
                      <td>{rent.getDate}</td>
                      <td>{rent.returnDate}</td>
                      <td>{"$" + rent.total}</td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateUser: (user) => dispatch(updateUser(user)),
  };
};

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
