import React from "react";
import { Link } from "react-router-dom";

const CarItem = ({ price, src, alt, link }) => {
  return (
    <div className="card-item__container">
      <Link to={link}>
        <h5>{price}</h5>
        <img src={src} alt={alt}></img>
        <button className="button__simple" type="button">
          View
        </button>
      </Link>
    </div>
  );
};

export default CarItem;
