import React from "react";
import { Link } from "react-router-dom";
import SignedInLinks from "./SignedInLinks";
import { connect } from "react-redux";
import SignedOutLinks from "./SignedOutLinks";

const Navbar = ({ openMenu, setOpenMenu, authentication }) => {
  const links = authentication.uid ? (
    <SignedInLinks openMenu={openMenu} setOpenMenu={setOpenMenu} />
  ) : (
    <SignedOutLinks openMenu={openMenu} setOpenMenu={setOpenMenu} />
  );
  return (
    <nav>
      <ul className="navbar navbar__container">
        <li className="navbar__logo">
          <Link to="/">
            <img src="/images/logo.png" alt="carrental"></img>
          </Link>
        </li>
        <li className="navbar__toggle" onClick={() => setOpenMenu(!openMenu)}>
          <span className="navbar__bars"></span>
        </li>
      </ul>
      {openMenu ? links : null}
    </nav>
  );
};

const mapStateToProps = (state) => {
  return {
    authentication: state.firebase.auth,
  };
};

export default connect(mapStateToProps)(Navbar);
