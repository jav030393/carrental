import React from "react";
import { NavLink } from "react-router-dom";

const SignedOutLinks = ({ openMenu, setOpenMenu }) => {
  return (
    <ul className="navbar__links">
      <li className="navbar__item">
        <NavLink onClick={() => setOpenMenu(!openMenu)} to="/signin">
          Log in
        </NavLink>
      </li>
      <li className="navbar__item">
        <NavLink onClick={() => setOpenMenu(!openMenu)} to="/signup">
          Sign up
        </NavLink>
      </li>
    </ul>
  );
};

export default SignedOutLinks;
