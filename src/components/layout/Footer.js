import React from "react";

const Footer = () => {
  return (
    <div className="footer__container">
      <div className="footer__body">
        <div className="footer__section">
          <div className="footer__text-group">
            <h4>PHONE</h4>
            <p>+507 68198188</p>
          </div>
          <div className="footer__text-group">
            <h4>EMAIL</h4>
            <p>javierbaromorales@gmail.com</p>
          </div>
        </div>
        <div className="footer__section">
          <div className="footer__text-group">
            <h4>ADDRESS</h4>
            <p>Plaza Camino de Cruces El Dorado, PB L#30</p>
          </div>
        </div>
        <div className="footer__section">
          <div className="footer__text-group">
            <img src="/images/logo-white.png" alt="Rent Car"></img>
            <p>
              Sabemos que para nuestros clientes, conformarse no es una opción.
              Es por eso que hoy brindamos a los clientes la oportunidad de
              disfrutar más la carretera. Líneas perfectas, lujosos interiores,
              y emocionantes muestras de potencia que le permiten disfrutar de
              una experiencia perfecta.
            </p>
          </div>
        </div>
      </div>

      <div className="footer__attach">
        <div className="footer__attach-background"></div>
        <div className="footer__attach-text">
          <h3>RENT</h3>
        </div>
      </div>
    </div>
  );
};

export default Footer;
