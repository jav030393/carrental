import React from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { signOut } from "../../store/actions/authActions";

const SignedInLinks = ({ signOut, openMenu, setOpenMenu }) => {
  const handleLogOutOnClick = (e) => {
    setOpenMenu(!openMenu);
    signOut();
  };
  return (
    <ul className="navbar__links">
      <li className="navbar__item">
        <a onClick={handleLogOutOnClick}>Log Out</a>
      </li>
      <li className="navbar__item">
        <NavLink onClick={() => setOpenMenu(!openMenu)} to="/profile">
          Profile
        </NavLink>
      </li>
    </ul>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut()),
  };
};

export default connect(null, mapDispatchToProps)(SignedInLinks);
