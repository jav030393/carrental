import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./styles/main.scss";
import Navbar from "./components/layout/Navbar";
import Footer from "./components/layout/Footer";
import Home from "./components/view/Home";
import CarDetail from "./components/view/CarDetail";
import SignIn from "./components/view/SignIn";
import SignUp from "./components/view/SignUp";
import Pay from "./components/view/Pay";
import Profile from "./components/view/Profile";

class App extends Component {
  state = {
    openMenu: false,
  };
  setOpenMenu = (open) => {
    this.setState({
      openMenu: open,
    });
  };
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar
            openMenu={this.state.openMenu}
            setOpenMenu={this.setOpenMenu}
          />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/car/:id" component={CarDetail} />
            <Route path="/signin" component={SignIn} />
            <Route path="/signup" component={SignUp} />
            <Route path="/pay/:id" component={Pay} />
            <Route path="/profile" component={Profile} />
          </Switch>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
