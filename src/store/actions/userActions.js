export const updateRentHistory = (rentInfo) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firestore = getFirestore();

    firestore
      .collection("user")
      .doc(getState().firebase.auth.uid)
      .set(rentInfo)
      .then(() => {
        dispatch({ type: "UPDATE_RENT_SUCCESS" });
      })
      .catch((err) => {
        dispatch({ type: "UPDATE_RENT_ERROR", err });
      });
  };
};
