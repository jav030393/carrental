export const signIn = (credentials) => {
  return (dispatch, getStatem, { getFirebase }) => {
    const firebase = getFirebase();

    firebase
      .auth()
      .signInWithEmailAndPassword(credentials.email, credentials.password)
      .then(() => {
        dispatch({ type: "LOGIN_SUCCESS" });
      })
      .catch((err) => {
        dispatch({ type: "LOGIN_ERROR", err });
      });
  };
};

export const signOut = (credentials) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();

    firebase
      .auth()
      .signOut()
      .then(() => {
        dispatch({ type: "SIGNOUT_SUCCESS" });
      });
  };
};

export const signUp = (newUser) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();

    firebase
      .auth()
      .createUserWithEmailAndPassword(newUser.email, newUser.password)
      .then((resp) => {
        return firestore.collection("user").doc(resp.user.uid).set({
          firstName: newUser.firstName,
          lastName: newUser.lastName,
          birthDate: newUser.birthDate,
          identification: newUser.identification,
          creditCard: newUser.creditCard,
          creditCardNumber: newUser.creditCardNumber,
          rentHistory: [],
        });
      })
      .then(() => {
        dispatch({ type: "SIGNUP_SUCCESS" });
      })
      .catch((err) => {
        dispatch({ type: "SIGNUP_ERROR", err });
      });
  };
};

export const updateUser = (updateUser) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();

    firebase
      .auth()
      .currentUser.updatePassword(updateUser.password)
      .then((resp) => {
        return firestore.collection("user").doc(resp.user.uid).set({
          firstName: updateUser.firstName,
          lastName: updateUser.lastName,
          birthDate: updateUser.birthDate,
          identification: updateUser.identification,
          creditCard: updateUser.creditCard,
          creditCardNumber: updateUser.creditCardNumber,
          rentHistory: updateUser.rentHistory,
        });
      })
      .then(() => {
        dispatch({ type: "UPDATE_USER_SUCCESS" });
      })
      .catch((err) => {
        dispatch({ type: "SIGNUP_USER_ERROR", err });
      });
  };
};
