"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateUser = void 0;

var updateUser = function updateUser(user) {
  return function (dispatch, getState, _ref) {
    var getFirebase = _ref.getFirebase,
        getFirestore = _ref.getFirestore;
    var firestore = getFirestore();
    firestore.collection('user').doc(user.id).update(user).then(function () {
      dispatch({
        type: 'USER_UPDATED',
        user: user
      });
    })["catch"](function (err) {
      dispatch({
        type: 'USER_UPDATED_ERROR',
        err: err
      });
    });
  };
};

exports.updateUser = updateUser;