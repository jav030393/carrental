"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateUser = exports.signUp = exports.signOut = exports.signIn = void 0;

var signIn = function signIn(credentials) {
  return function (dispatch, getStatem, _ref) {
    var getFirebase = _ref.getFirebase;
    var firebase = getFirebase();
    firebase.auth().signInWithEmailAndPassword(credentials.email, credentials.password).then(function () {
      dispatch({
        type: "LOGIN_SUCCESS"
      });
    })["catch"](function (err) {
      dispatch({
        type: "LOGIN_ERROR",
        err: err
      });
    });
  };
};

exports.signIn = signIn;

var signOut = function signOut(credentials) {
  return function (dispatch, getState, _ref2) {
    var getFirebase = _ref2.getFirebase;
    var firebase = getFirebase();
    firebase.auth().signOut().then(function () {
      dispatch({
        type: "SIGNOUT_SUCCESS"
      });
    });
  };
};

exports.signOut = signOut;

var signUp = function signUp(newUser) {
  return function (dispatch, getState, _ref3) {
    var getFirebase = _ref3.getFirebase,
        getFirestore = _ref3.getFirestore;
    var firebase = getFirebase();
    var firestore = getFirestore();
    firebase.auth().createUserWithEmailAndPassword(newUser.email, newUser.password).then(function (resp) {
      return firestore.collection("user").doc(resp.user.uid).set({
        firstName: newUser.firstName,
        lastName: newUser.lastName,
        birthDate: newUser.birthDate,
        identification: newUser.identification,
        creditCard: newUser.creditCard,
        creditCardNumber: newUser.creditCardNumber,
        rentHistory: []
      });
    }).then(function () {
      dispatch({
        type: "SIGNUP_SUCCESS"
      });
    })["catch"](function (err) {
      dispatch({
        type: "SIGNUP_ERROR",
        err: err
      });
    });
  };
};

exports.signUp = signUp;

var updateUser = function updateUser(_updateUser) {
  return function (dispatch, getState, _ref4) {
    var getFirebase = _ref4.getFirebase,
        getFirestore = _ref4.getFirestore;
    var firebase = getFirebase();
    var firestore = getFirestore();
    firebase.auth().currentUser.updatePassword(_updateUser.password).then(function (resp) {
      return firestore.collection("user").doc(resp.user.uid).set({
        firstName: _updateUser.firstName,
        lastName: _updateUser.lastName,
        birthDate: _updateUser.birthDate,
        identification: _updateUser.identification,
        creditCard: _updateUser.creditCard,
        creditCardNumber: _updateUser.creditCardNumber,
        rentHistory: _updateUser.rentHistory
      });
    }).then(function () {
      dispatch({
        type: "UPDATE_USER_SUCCESS"
      });
    })["catch"](function (err) {
      dispatch({
        type: "SIGNUP_USER_ERROR",
        err: err
      });
    });
  };
};

exports.updateUser = updateUser;