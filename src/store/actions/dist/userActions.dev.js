"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateRentHistory = void 0;

var updateRentHistory = function updateRentHistory(rentInfo) {
  return function (dispatch, getState, _ref) {
    var getFirebase = _ref.getFirebase,
        getFirestore = _ref.getFirestore;
    var firestore = getFirestore();
    firestore.collection("user").doc(getState().firebase.auth.uid).set(rentInfo).then(function () {
      dispatch({
        type: "UPDATE_RENT_SUCCESS"
      });
    })["catch"](function (err) {
      dispatch({
        type: "UPDATE_RENT_ERROR",
        err: err
      });
    });
  };
};

exports.updateRentHistory = updateRentHistory;