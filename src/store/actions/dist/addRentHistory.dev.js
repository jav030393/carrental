"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addRentHistory = void 0;

var addRentHistory = function addRentHistory(rentHistory) {
  return function (dispatch, getState, _ref) {
    var getFirebase = _ref.getFirebase,
        getFirestore = _ref.getFirestore;
    dispatch('ADD_RENT_HISTORY', rentHistory);
  };
};

exports.addRentHistory = addRentHistory;