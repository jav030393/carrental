import authReducer from "./authReducer";
import userReducer from "./userReducer";
import carReducer from "./carReducer";
import { combineReducers } from "redux";
import { firestoreReducer } from "redux-firestore";
import { firebaseReducer } from "react-redux-firebase";

const rootReducer = combineReducers({
  auth: authReducer,
  user: userReducer,
  car: carReducer,
  firestore: firestoreReducer,
  firebase: firebaseReducer,
});

export default rootReducer;
