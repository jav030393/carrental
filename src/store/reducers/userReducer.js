const initSate = {
  payMessage: null,
};

const carReducer = (state = initSate, action) => {
  switch (action.type) {
    case "UPDATE_RENT_SUCCESS":
      return {
        ...state,
        payMessage: "Pay Success",
      };
    case "UPDATE_RENT_ERROR":
      return {
        ...state,
        payMessage: null,
      };
    default:
      return state;
  }
};

export default carReducer;
