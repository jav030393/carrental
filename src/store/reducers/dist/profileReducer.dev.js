"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var initSate = {
  profiles: [{
    id: 1,
    firstName: 'Javier',
    lastName: 'Baro',
    birthDate: 27,
    identification: 'i3243',
    creditCard: 1,
    creditCardNumber: '45345234534',
    rentHistory: [{
      id: 1,
      src: '/images/audi.png',
      alt: 'Audi',
      getDate: "03/03/2020",
      returnDate: "10/03/2020",
      total: 123.3
    }, {
      id: 2,
      src: '/images/bmw.png',
      alt: 'BMW',
      getDate: "03/03/2019",
      returnDate: "10/03/2019",
      total: 500.3
    }]
  }]
};

var profileReducer = function profileReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initSate;
  var action = arguments.length > 1 ? arguments[1] : undefined;
  return state;
};

var _default = profileReducer;
exports["default"] = _default;