"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initSate = {
  authError: null
};

var authReducer = function authReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initSate;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case "LOGIN_ERROR":
      return _objectSpread({}, state, {
        authError: "Login fail"
      });

    case "LOGIN_SUCCESS":
      return _objectSpread({}, state, {
        authError: null
      });

    case "LOGOUT_SUCCESS":
      return state;

    case "SIGNUP_SUCCESS":
      return _objectSpread({}, state, {
        authError: null
      });

    case "SIGNUP_ERROR":
      return _objectSpread({}, state, {
        authError: action.err.message
      });

    case "UPDATE_USER_SUCCESS":
      return _objectSpread({}, state, {
        userUpadateError: "Update user success"
      });

    case "UPDATE_USER_ERROR":
      return _objectSpread({}, state, {
        userUpadateError: null
      });

    default:
      return state;
  }
};

var _default = authReducer;
exports["default"] = _default;