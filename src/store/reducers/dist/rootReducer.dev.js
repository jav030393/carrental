"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _authReducer = _interopRequireDefault(require("./authReducer"));

var _userReducer = _interopRequireDefault(require("./userReducer"));

var _carReducer = _interopRequireDefault(require("./carReducer"));

var _redux = require("redux");

var _reduxFirestore = require("redux-firestore");

var _reactReduxFirebase = require("react-redux-firebase");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var rootReducer = (0, _redux.combineReducers)({
  auth: _authReducer["default"],
  user: _userReducer["default"],
  car: _carReducer["default"],
  firestore: _reduxFirestore.firestoreReducer,
  firebase: _reactReduxFirebase.firebaseReducer
});
var _default = rootReducer;
exports["default"] = _default;