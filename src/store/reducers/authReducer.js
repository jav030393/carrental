const initSate = {
  authError: null,
};

const authReducer = (state = initSate, action) => {
  switch (action.type) {
    case "LOGIN_ERROR":
      return {
        ...state,
        authError: "Login fail",
      };
    case "LOGIN_SUCCESS":
      return {
        ...state,
        authError: null,
      };
    case "LOGOUT_SUCCESS":
      return state;
    case "SIGNUP_SUCCESS":
      return {
        ...state,
        authError: null,
      };
    case "SIGNUP_ERROR":
      return {
        ...state,
        authError: action.err.message,
      };
    case "UPDATE_USER_SUCCESS":
      return {
        ...state,
        userUpadateError: "Update user success",
      };
    case "UPDATE_USER_ERROR":
      return {
        ...state,
        userUpadateError: null,
      };
    default:
      return state;
  }
};

export default authReducer;
